/**
 * triangle 1.js
 * 
 * Affiche un motif triangulaire dont la taille est fixée par une valeur lue au
 * clavier.
 *
 * Par exemple si la taille donnée est 7, le motif affiché sera :
 *
 *    *
 *    **
 *    ***
 *    ****
 *    *****
 *    ******
 *    *******
 * 
 * L'algorithme utilisé est un algorithme naif : on reconstruit chaque ligne ce qui nécessite
 * deux boucles imbriquées. Cela peut être simplifié (voir le programme triangle1bis.js). 
 * Il est implémenté en n'utilisant que des boucles while.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes du triangle : ");
let noLigne = 1;
while (noLigne <= nbLignes) {
    // construction de la ligne à afficher, c'est à dire d'une chaîne 
    // constituée d'un nombre d'étoiles identique au numéro de ligne
    let ligne = "";
    let i = 1;
    while (i <= noLigne) {
        ligne += "*";
        i++;
    }
    // on affiche la ligne
    console.log(ligne);
    // on passe à la ligne suivante
    noLigne++;
}