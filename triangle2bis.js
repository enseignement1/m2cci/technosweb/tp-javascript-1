/**
 * triangle2bis.js
 * 
 * Même algorithme que triangle1bis.js, mais implémenté avec des boucles for
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes du triangle : ");
let etoiles = "*";
for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // affiche la ligne noLigne, cela consiste à afficher un nombre d'étoiles ('*') égal à noLigne
    console.log(etoiles);
    // on rajoute une étoile pour la ligne suivante
    etoiles += "*";
}