/**
 * triangle1bis.js
 * 
 * Simplification de l'algorithme du programme triangle1.js. En constatant
 * que chaque ligne comporte une étoile de plus que la ligne précédente
 * on peut supprimer la boucle intérieure qui construisait la ligne d'étoiles
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes du triangle : ");

let noLigne = 1;  // n° de la ligne à afficher
let ligne = "*" // chaîne constituée d'un nombre d'étoiles identique à noLigne
while (noLigne <= nbLignes) {
    // affiche la ligne noLigne
    console.log(ligne);
    // on passe à la ligne suivante
    noLigne++;
    ligne = ligne + "*"; // la ligne suivante aura une étoile de plus
}