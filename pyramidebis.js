/**
 * Simplification de l'algorithme du programme pyramide.js. En constatant
 * que chaque ligne comporte deux étoiles de plus et un espace de moins
 * que la ligne précédenteb on peut supprimer les boucles intérieure qui 
 * construisaient la ligne (espaces et étoiles)
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes de la pyramide : ");
let etoiles = "*";
let espaces = ""; // chaine vide
for (let i = 0; i < nbLignes; i++) {
    espaces += " ";
}
for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // affiche la ligne noLigne 
    // substring permet d'obtenir un nombre d'espaces (' ') égal à nbLignes - noLigne
    console.log(espaces.substring(0,nbLignes - noLigne) + etoiles)
    // la ligne suivante aura deux étoiles de plus
    etoiles = etoiles + "**";
}