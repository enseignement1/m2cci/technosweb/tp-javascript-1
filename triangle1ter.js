/**
 * triangle1ter.js
 * 
 * Une autre variation de l'algorithme triangle1.js. Ici pour la génération de la ligne d'étoile
 * on utilise la méthode (fonction) repeat des objets String.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes du triangle : ");

let noLigne = 1;  // n° de la ligne à afficher
while (noLigne <= nbLignes) {
    // affiche la ligne noLigne, cela consiste à afficher un nombre d'étoiles ('*') égal à noLigne
    console.log("*".repeat(noLigne));
    // on passe à la ligne suivante
    noLigne++;
}