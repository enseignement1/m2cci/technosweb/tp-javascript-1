/**
 * triangle2ter.js
 *
 *  Même algorithme que triangle1ter.js, mais implémenté avec des boucles for
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes du triangle : ");
for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // affiche la ligne noLigne, cela consiste à afficher un nombre d'étoiles ('*') égal à noLigne
    console.log("*".repeat(noLigne));
}