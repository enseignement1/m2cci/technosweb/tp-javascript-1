/**
 * Calcule la moyenne olympique d'une suite de notes.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync';

const FIN_SEQUENCE = -1; // valeur marquant la fin d'une séquence
const NOTE_MIN = 0; // note minimale autorisée
const NOTE_MAX = 10; // note maximale autorisée

/**
 * Fonction utilitaire pour lire une note au clavier. On vérifie que la note est
 * bien dans l'intervalle de notes autorisé ou que c'est la valeur de fin de séquence
 * pour terminer la saisie. Dans ce cas on vérifie que trois notes au moins ont déjà
 * été saisies. Si la note est incorrecte, ou si la valeur de fin de séquence est
 * rentrée et qu'il n'y a pas encore au moins trois notes de saisies, on redemande la saisie
 * de la note.
 * @param {number} nbNotesLues le nombre de notes qui ont déjà été lues et traitées.
 * @param {number} valFinSeq la valeur marquant la fin de séquence
 * @param {number} valMin la borne inférieure de l'intervalle de valeurs
 * @param {number} valMax la borne supérieure de l'intervalle de valeurs
 * @returns la note lue
 */
function lireUneNote(nbNotesLues, valFinSeq, valMin, valMax) {
  let noteLue;
  let noteOK = false;
  do {
    noteLue = readline.questionFloat(
      `Donner un note entre ${valMin} et ${valMax} (${valFinSeq} pour terminer la saisie) : `
    );
    if (noteLue !== valFinSeq && (noteLue < valMin || noteLue > valMax)) {
      console.log(
        `Valeur incorrecte ! Donnez une note entre ${valMin} et ${valMax} (${valFinSeq} pour arrêter)`
      );
      noteOK = false;
    } else {
      // noteLue === valFinSeq || (noteLue >= valMin && noteLue <= valMax)
      // noteLue est une note valide ou la marque de fin de séquence
      if (noteLue === valFinSeq && nbNotesLues < 3) {
        console.log("vous devez rentrez au moins trois notes");
        noteOK = false;
      } else {
        noteOK = true;
      }
    }
  } while (!noteOK);
  return noteLue;
}

//---------------------------------------------
// Le programme principal
//---------------------------------------------
let total = 0.0; // le cumul des notes saisies
let nbNotes = 0; // le nombre de notes saisies
let max = Number.MIN_VALUE; // la note la plus forte
// initialisée avec la plus petite valeur codable pour un nombre
let min = Number.MAX_VALUE; // la note la plus faible
// initialisée avec la plus grande valeur codable pour un double

let note = lireUneNote(0, FIN_SEQUENCE, NOTE_MIN, NOTE_MAX);

while (note != FIN_SEQUENCE) {
  total += note;
  if (max <= note) {
    max = note;
  }
  if (min >= note) {
    // on ne met pas un else if car la première note sera à la fois min et max
    min = note;
  }
  nbNotes++;

  note = lireUneNote(nbNotes, FIN_SEQUENCE, NOTE_MIN, NOTE_MAX);
}

console.log("\nNombre de notes saisies " + nbNotes);
console.log(
  `La note la plus élevée (${max}) et la note plus basse (${min}) ont été retirées`
);
console.log(
  "La moyenne olympique est : " + (total - (min + max)) / (nbNotes - 2)
);
