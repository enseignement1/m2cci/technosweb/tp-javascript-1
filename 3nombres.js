/**
 * 3nombres.js
 *
 * Lit au clavier trois valeurs entières et les affiche dans l'ordre croissant
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let n1 = readline.questionInt("1er nombre : ");
let n2 = readline.questionInt("2ème nombre : ");
let n3 = readline.questionInt("3ème nombre : ");
let res = "";
if (n1 < n2) {    // n1 < n2
    if (n1 < n3) {  // n1 < n2 et n1 < n3 il faut comparer n2 avec n3
        if (n2 < n3) {  n1 < n2 < n3
            res = `${n1} ${n2} ${n3}`;
        } else {  // n1 <= n3 < n2
            res = `${n1} ${n3} ${n2}`;
        }
    } else { // n1 < n2 et n3 <= n1
        res = `${n3} ${n1} ${n2}`
    }
} else {  // n2 <= n1
    if (n1 < n3) {   // n2 <= n1 < n3
        res = `${n2} ${n1} ${n3}`
    } else {  // n2 <= n1 et n1 >= n3
        if (n2 < n3) {    // n2 < n3 <= n1
            res = `${n2} ${n3} ${n1}`
        }
        else {  // n3 <= n2 <= n1
            res = `${n3} ${n2} ${n1}`
        }
    }
}
console.log("les 3 nombres dans l'ordre croissant : " + res);