/**
 * triangle2.js
 * 
 * Même algorithme que triangle1.js, mais implémenté avec des boucles for
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;
let nbLignes = readline.questionInt("nombre de lignes du triangle : ");
let noLigne = 1;

for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // affiche la ligne noLigne
    // cela consiste à afficher noLigne '*'
    let ligne = "*";
    for (let i = 2; i <= noLigne; i++) {
        ligne += "*";
    }
    console.log(ligne);
}