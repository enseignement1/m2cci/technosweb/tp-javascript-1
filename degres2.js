/*
 * Reprend le programme degres.js de conversion de températures de ° Fahreinheit vers ° Celsius
 * mais il utilise la fonction utilitaire encore() (définie dans le module utils)
 * afin que l'utilisteur puisse si il le souhaite effectuer plusieurs conversions
 * successives.
 *  
 * Pour les lectures au clavier le module readline-sync est utilisé
 *
 * voir https://www.npmjs.com/package/readline-sync
 */

import readline  from 'readline-sync'; // pour utiliser le module readline-sync
import { encore }  from './utils.js'; // pour utiliser le fonction encore du module utils

/**
 * convertit une valeur de degrès Fahreinheit vers les degrès Celsius
 * @param {number} tempF la valeur à convertir en degrés Fahreinheit
 * @returns {number} la valeur en degrés Celsius
 */
function fahreinheit2Celsius(tempF) {
    return (5 / 9) * (tempF - 32);
}

//----------------------------
// Le programme principal
//----------------------------
do {
    let tempF = readline.questionFloat("donnez une température en degrés Fahrenheit : ");
    console.log(`la température en degrés Celsius est ${fahreinheit2Celsius(tempF).toFixed(2)} °C`);
} while (encore("voulez-vous recommencer ? "));
console.log("Au revoir !");