/**
 * hjms2.js
 *
 * Reprend le programme hjms1.js qui lit au clavier une durée exprimée en secondes et la convertit
 * en nombre de jours, heures, minutes et secondes. Ce programme est amélioré de sorte que 
 * lorsqu'une valeur (nombre de jours, d'heures , de minutes ou de secondes) est nulle elle 
 * n'apparait pas dans l'affichage, et que si elle vaut 1 l'unité est affichée au singulier (sans s) 
 * comme dans l'exemple ci-dessous. 
 * 
 * >node hjms1.js 
 * donnez une durée en secondes : 3621 
 * Cette durée equivaut à 1 heure 21 secondes 
 * >
 * 
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

/**
 * renvoie une chaîne affichant un nombre positif avec son unité au singulier
 * ou au pluriel selon que le nombre est supérieur à 9 ou non et une chaîne
 * vide si le nombre est nul
 * @param {number} n un nombre (>= 0)
 * @param {string} unite (l'unité associée à ce nombre)
 * @returns une chaîne de caractères
 *      - '' vide si n = 0
 *      - '1 unite ' si n est égal à 1
 *      - 'n unites ' si n est supérieur à 1
 */
function formatPluriel(n, unite) {
  if (n > 0) {
    let res = n + " " + unite;
    if (n > 1) {
      res += "s";
    }
    return res + " ";
  }
  return "";
}

// le programme principal

const NB_SEC_PAR_MINUTE = 60;
const NB_SEC_PAR_HEURE = 60 * NB_SEC_PAR_MINUTE;
const NB_SEC_PAR_JOUR = 24 * NB_SEC_PAR_HEURE;

let durée = readline.questionInt("entrez une durée (en sec.) : ");

let nbJours = Math.floor(durée / NB_SEC_PAR_JOUR); //l'opérateur '/' est l'opérateur de division entière
durée = durée % NB_SEC_PAR_JOUR; //l'opérateur '%' est l'opérateur modulo, reste de la division entière
let nbHeures = Math.floor(durée / NB_SEC_PAR_HEURE);
durée = durée % NB_SEC_PAR_HEURE;
let nbMin = Math.floor(durée / NB_SEC_PAR_MINUTE);
let nbSec = durée % NB_SEC_PAR_MINUTE;

console.log(
  "Cette durée equivaut à " +
    formatPluriel(nbJours, "jour") +
    formatPluriel(nbHeures, "heure") +
    formatPluriel(nbMin, "minute") +
    formatPluriel(nbSec, "seconde")
);
