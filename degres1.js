/*
 * Conversion de températures de ° Fahreinheit vers ° Celsius
 * Pour les lectures au clavier le module readline-sync est utilisé
 *
 * voir https://www.npmjs.com/package/readline-sync
 */

// pour utiliser le module readline
import readline from 'readline-sync'; // syntaxe ESM (ECMA Script Modules)

let tempF = readline.questionFloat("donnez une température en degrès Fahrenheit : ");
let tempC = (5 / 9) * (tempF -32);
console.log(`la température en degrés Celsius est ${tempC.toFixed(2)} °C`);