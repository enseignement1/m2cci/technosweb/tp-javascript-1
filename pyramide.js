/**
 * Affiche un motif pyramidal dont la taille est fixée par une valeur lue au
 * clavier.
 *
 * Par exemple si la taille donnée est 7, le motif affiché sera :
 *
 *          *
 *         ***
 *        *****
 *       *******
 *      *********
 *     ***********
 *    *************
 *
 * L'algorithme utilisé est un algorithme naif : on reconstruit chaque ligne ce qui nécessite
 * deux boucles imbriquées. Cela peut être simplifié (voir le programme pyramidebis.js). 
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes de la pyramide : ");
for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // construit la ligne noLigne 
    // un nombre d'espaces (' ') égal à nbLignes - noLigne
    let espaces = ""; // chaine vide
    for (let i = 0; i < nbLignes - noLigne; i++) {
        espaces += " ";
    }
    // un nombre d'étoiles égal à 2 * noLigne - 1
    let etoiles = ""; // chaine vide
    for (let i = 0; i < 2 * noLigne - 1; i++) {
        etoiles += "*";
    }
    // affiche la ligne
    console.log(espaces + etoiles)
}