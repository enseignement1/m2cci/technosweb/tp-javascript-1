/**
 * Affiche un motif triangulaire dont la taille (nombre de lignes) est fixée par une valeur lue au
 * clavier.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */
import readline from 'readline-sync' ;

let nbLignes = readline.questionInt("nombre de lignes de la pyramide : ");
for (let noLigne = 1; noLigne <= nbLignes; noLigne++) {
    // affiche la ligne noLigne, cela consiste à afficher un nombre d'étoiles ('*') 
    // égal à 2 * noLigne -1 précédé d'un nombre d'espaces (' ') égal à nbLignes - noLigne
    console.log(" ".repeat(nbLignes - noLigne) + "*".repeat(2* noLigne - 1));
}
