/**
 * hjms1.js
 *
 * Lit au clavier une durée exprimée en secondes et la convertit en nombre de
 * jours, heures, minutes et secondes.
 *
 * @author Philippe Genoud - Equipe STeamer - LIG - Université Grenoble Alpes
 */

import readline from "readline-sync"; // pour utiliser le module readline-sync

const NB_SEC_PAR_MINUTE = 60;
const NB_SEC_PAR_HEURE = 60 * NB_SEC_PAR_MINUTE;
const NB_SEC_PAR_JOUR = 24 * NB_SEC_PAR_HEURE;

let durée = readline.questionInt("entrez une durée (en sec.) : ");

let nbJours = Math.floor(durée / NB_SEC_PAR_JOUR); //l'opérateur '/' est l'opérateur de division entière
durée = durée % NB_SEC_PAR_JOUR; //l'opérateur '%' est l'opérateur modulo, reste de la division entière
let nbHeures = Math.floor(durée / NB_SEC_PAR_HEURE);
durée = durée % NB_SEC_PAR_HEURE;
let nbMin = Math.floor(durée / NB_SEC_PAR_MINUTE);
let nbSec = durée % NB_SEC_PAR_MINUTE;

console.log(
  `Cette durée équivaut à ${nbJours} jours  ${nbHeures} heures ${nbMin} minutes ${nbSec} secondes`
);
